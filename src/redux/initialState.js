import { defaultStyles, defaultTableName } from '@/constants';
import { clone } from '@core/utils';

const defaultState = {
  rowState: {},
  colState: {},
  dataState: {},
  currentText: '',
  currentStyles: defaultStyles,
  stylesState: {},
  tableName: defaultTableName,
  opened: new Date().toJSON(),
};

const normalize = (state) => ({
  ...state,
  currentStyles: defaultStyles,
  currentText: '',
});

// eslint-disable-next-line import/prefer-default-export
export function normalizeInitialState(state) {
  return state ? normalize(state) : clone(defaultState);
}
