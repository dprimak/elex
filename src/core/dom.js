class Dom {
  constructor(selector) {
    this.$el = typeof selector === 'string'
      ? document.querySelector(selector)
      : selector;
  }
  
  html(html) {
    if (typeof html === 'string') {
      this.$el.innerHTML = html;
      return this;
    }
    return this.$el.outerHTML.trim();
  }
  
  text(text) {
    if (typeof text !== 'undefined') {
      this.$el.textContent = text;
      return this;
    }
    
    if (this.$el.tagName.toLowerCase() === 'input') {
      return this.$el.value.trim();
    }
    
    return this.$el.textContent.trim();
  }
  
  clear() {
    this.html('');
    return this;
  }
  
  append(node) {
    if (node instanceof Dom) {
      // eslint-disable-next-line no-param-reassign
      node = node.$el;
    }
    if (Element.prototype.append) {
      this.$el.append(node);
    } else {
      this.$el.appendChild(node);
    }
    
    return this;
  }
  
  getStyles(styles = []) {
    return styles.reduce((res, s) => {
      res[s] = this.$el.style[s];
      return res;
    }, {});
  }
  
  get data() {
    return this.$el.dataset;
  }
  
  id(parse) {
    if (parse) {
      const parsed = this.id().split(':');
      return {
        col: +parsed[0],
        row: +parsed[1],
      };
    }
    return this.data.id;
  }
  
  css(styles = {}) {
    Object.keys(styles).forEach((key) => {
      this.$el.style[key] = styles[key];
    });
  }
  
  closest(selector) {
    // eslint-disable-next-line no-use-before-define
    return $(this.$el.closest(selector));
  }
  
  getCoords() {
    return this.$el.getBoundingClientRect();
  }
  
  findAll(selector) {
    return this.$el.querySelectorAll(selector);
  }
  
  find(selector) {
    // eslint-disable-next-line no-use-before-define
    return $(this.$el.querySelector(selector));
  }
  
  addClass(className) {
    this.$el.classList.add(className);
    return this;
  }
  
  removeClass(className) {
    this.$el.classList.remove(className);
    return this;
  }
  
  on(eventType, callback) {
    this.$el.addEventListener(eventType, callback);
  }
  
  off(eventType, listener) {
    this.$el.removeEventListener(eventType, listener);
  }
  
  focus() {
    this.$el.focus();
    return this;
  }
  
  attr(name, value) {
    this.$el.setAttribute(name, value);
    return this;
  }
}

// eslint-disable-next-line import/prefer-default-export
export function $(selector) {
  return new Dom(selector);
}

$.create = (tagName, classes = '') => {
  const el = document.createElement(tagName);
  if (classes) {
    el.classList.add(classes);
  }
  
  return $(el);
};
