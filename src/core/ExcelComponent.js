import DOMListener from './DOMListener';

export class ExcelComponent extends DOMListener {
  constructor($root, options = {}) {
    super($root, options.listeners);
    this.name = options.name || '';
    this.emitter = options.emitter;
    this.subscribe = options.subscribe || [];
    this.store = options.store;
    this.unsubscribes = [];
    this.id = options.id;
  
    this.prepare();
  }

  // Настраиваем наш компонент до init
  // eslint-disable-next-line class-methods-use-this
  prepare() {}
  
  // Инициализация компонента
  init() {
    this.initDOMListener();
  }
  
  $dispatch(action) {
    this.store.dispatch(action);
  }
  
  // Сюда приходят изменения по полям на которіе мі подписались
  // eslint-disable-next-line class-methods-use-this
  storeChanged() {}
  
  isWatching(key) {
    return this.subscribe.includes(key);
  }
  
  // Уведомляем слушателей про событие
  $emit(event, ...args) {
    this.emitter.emit(event, ...args);
  }
  
  // Подписіваемся на собітие
  $on(event, fn) {
    const unsub = this.emitter.subscribe(event, fn);
    this.unsubscribes.push(unsub);
  }
  
  // Удаляем компонента
  destroy() {
    this.removeDOMListener();
    this.unsubscribes.forEach((unsub) => unsub());
  }

  // Возвращает шаблон компонента
  // eslint-disable-next-line class-methods-use-this
  toHTML() {
    return '';
  }
}

export default ExcelComponent;
