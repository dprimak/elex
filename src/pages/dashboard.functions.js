import { storage } from '@core/utils';

function toHTML(key) {
  const id = key.split(':')[1];
  const model = storage(key);
  return `
    <li class="db__record">
        <a href="#excel/${id}">${model.tableName}</a>
        <strong>
            ${new Date(model.opened).toLocaleDateString()}
            ${new Date(model.opened).toLocaleTimeString()}
        </strong>
    </li>
  `;
}

export function getAllKeys() {
  const keys = [];
  
  for (let i = 0; i < localStorage.length; i += 1) {
    const key = localStorage.key(i);
    if (!key.includes('excel')) {
      // eslint-disable-next-line no-continue
      continue;
    }
    keys.push(key);
  }
  
  return keys;
}

export function createRecordsTable() {
  const keys = getAllKeys();
  
  if (!keys.length) {
    return '<p>Вы пока не создали ни одной таблицы</p>'; 
  }
  return `
    <div class="db__list-header">
        <span>Название</span>
        <span>Дата открытия</span>
    </div>
    <ul class="db__list">
        ${keys.map(toHTML).join('')}
    </ul>
  `;
}
