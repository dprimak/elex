export function isDelete($target) {
  return $target.data.button === 'remove';
}

export function isExit($target) {
  return $target.data.button === 'exit';
}
