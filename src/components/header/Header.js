import ExcelStateComponent from '@core/ExcelStateComponent';
import { changeTableName } from '@/redux/actions';
import { $ } from '@core/dom';
import { defaultTableName } from '@/constants';
import { isDelete, isExit } from '@/components/header/header.functions';
import { removeStorage, storageName } from '@core/utils';
import ActiveRoute from '@core/router/ActiveRoute';

class Header extends ExcelStateComponent {
  static className = 'excel__header';
  
  constructor($root, options) {
    super($root, {
      name: 'Header',
      listeners: ['input', 'click'],
      ...options,
    });
    this.title = this.store.getState().tableName || defaultTableName;
  }
  
  onInput(event) {
    const $target = $(event.target);
    this.$dispatch(changeTableName($target.text()));
  }
  
  onClick(event) {
    const $target = $(event.target);
    
    if (isExit($target)) {
      ActiveRoute.navigate('');
    } else if (isDelete($target)) {
      // eslint-disable-next-line no-restricted-globals,no-alert
      const modal = confirm(`Удалить таблицу '${this.title}'?`);
      if (modal) {
        removeStorage(storageName(this.id));
        ActiveRoute.navigate('');
      }
    }
  }

  toHTML() {
    return `
      <input type="text" class="input" value="${this.title}" />
      <div>
          <div class="button" data-button="remove">
              <i class="material-icons" data-button="remove">delete</i>
          </div>
          <div class="button" data-button="exit">
              <i class="material-icons" data-button="exit">exit_to_app</i>
          </div>
      </div>
    `;
  }
}

export default Header;
