import { ragne } from '@core/utils';

export function shouldResize(event) {
  return event.target.dataset.resize;
}

export function isCell(event) {
  return event.target.dataset.id;
}

export function matrix($current, $target) {
  const target = $target.id(true);
  const current = $current.id(true);
  const cols = ragne(current.col, target.col);
  const rows = ragne(current.row, target.row);
  return cols.reduce((acc, col) => {
    rows.forEach((row) => acc.push(`${col}:${row}`));
    return acc;
  }, []);
}

export function nextSelector(key, { col, row }) {
  const MIN_VALUE = 0;
  // eslint-disable-next-line default-case
  switch (key) {
    case 'Enter':
    case 'ArrowDown':
      // eslint-disable-next-line no-plusplus,no-param-reassign
      row++;
      break;
    case 'Tab':
    case 'ArrowRight':
      // eslint-disable-next-line no-plusplus,no-param-reassign
      col++;
      break;
    case 'ArrowLeft':
      // eslint-disable-next-line no-plusplus,no-param-reassign
      col = col - 1 < MIN_VALUE ? MIN_VALUE : col - 1;
      break;
    case 'ArrowUp':
      // eslint-disable-next-line no-plusplus,no-param-reassign
      row = row - 1 < MIN_VALUE ? MIN_VALUE : row - 1;
      break;
  }
  
  return `[data-id="${col}:${row}"]`;
}
