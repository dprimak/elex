import { $ } from '@core/dom';

// eslint-disable-next-line import/prefer-default-export
export function resizeHandler($root, event) {
  return new Promise((resolve) => {
    let value;
    const type = event.target.dataset.resize;
    const $resize = $(event.target);
    const $parent = $resize.closest('[data-type="resizable"]');
    const arrayCol = $root.findAll(`[data-col="${$parent.data.col}"]`);
    const coords = $parent.getCoords();
    const sideProp = type === 'col' ? 'bottom' : 'right';
  
    $resize.css({
      opacity: 1,
      [sideProp]: '-5000px',
    });
  
    document.onmousemove = (e) => {
      if (type === 'col') {
        const delta = e.pageX - coords.right;
        value = coords.width + delta;
        $resize.css({
          right: `${-delta}px`,
          opacity: 1,
        });
      } else {
        const delta = e.pageY - coords.bottom;
        value = coords.height + delta;
        $resize.css({
          bottom: `${-delta}px`,
          opacity: 1,
        });
      }
    };
  
    document.onmouseup = () => {
      if (type === 'col') {
        $parent.css({ width: `${value}px` });
        arrayCol.forEach((el) => {
          // eslint-disable-next-line no-param-reassign
          el.style.width = `${value}px`;
        });
      } else {
        $parent.css({ height: `${value}px` });
      }
    
      $resize.css({
        opacity: 0,
        right: 0,
        bottom: 0,
      });
    
      document.onmousemove = null;
      document.onmouseup = null;
      
      resolve({
        value,
        type,
        id: $parent.data[type],
      });
    };
  });
}
