import { defaultStyles } from '@/constants';
import { toInlineStyles } from '@core/utils';
import { parse } from '@core/parse';

const CODES = {
  A: 65,
  Z: 90,
};

const DEFAULT_WIDTH = 120;
const DEFAULT_HEIGHT = 24;

function getWidth(state, id) {
  return state[id] ? `${state[id]}px` : `${DEFAULT_WIDTH}px`;
}

function getHeight(state, id) {
  return state[id] ? `${state[id]}px` : `${DEFAULT_HEIGHT}px`;
}

function withWidthFrom(state) {
  // eslint-disable-next-line func-names
  return function (col, index) {
    return {
      col, index, width: getWidth(state, index),
    };
  };
}

function createCell(row, state) {
  // eslint-disable-next-line func-names
  return function (_, col) {
    const id = `${col}:${row}`;
    const width = getWidth(state.colState, col);
    const text = state.dataState[`${col}:${row}`];
    const styles = toInlineStyles({ ...defaultStyles, ...state.stylesState[id] });
    return `
        <div class="cell"
         contenteditable
         data-col="${col}"
         data-id="${id}"
         data-value="${text || ''}"
         style="${styles}; width: ${width}"
        >${parse(text) || ''}</div>
    `;
  };
}

function createCol({ col, index, width }) {
  return `
    <div class="column" data-type="resizable" data-col="${index}" style="width: ${width}">
        ${col}
        <div class="col-resize" data-resize="col"></div>
    </div>
  `;
}

function createRow(index, data, rowState) {
  const resize = index ? '<div class="row-resize" data-resize="row"></div>' : '';
  const row = (index) || '';
  const height = getHeight(rowState, row);
  return `
    <div class="row" data-type="resizable" data-row="${row}" style="height: ${height}">
      <div class="row-info">
        ${index || ''}
        ${resize}
      </div>
      <div class="row-data">${data}</div>
    </div>
  `;
}

function toChar(_, index) {
  return String.fromCharCode(CODES.A + index);
}

// eslint-disable-next-line import/prefer-default-export
export function createTable(rowsCount = 10, state = {}) {
  const colsCount = CODES.Z - CODES.A + 1;
  const { colState, rowState } = state;
  
  const rows = [];
  
  const cols = new Array(colsCount).fill('')
    .map(toChar)
    .map(withWidthFrom(colState))
    .map(createCol)
    .join('');
  
  rows.push(createRow(null, cols, {}));
  
  // eslint-disable-next-line no-plusplus
  for (let row = 0; row < rowsCount; row++) {
    const cells = new Array(colsCount).fill('')
      .map(createCell(row, state))
      .join('');
    rows.push(createRow(row + 1, cells, rowState));
  }
  
  return rows.join('');
}
